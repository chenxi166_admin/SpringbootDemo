package com.temdemo.controller;

import com.temdemo.service.UserService;
import com.temdemo.utils.BaseController;
import com.temdemo.utils.restful.AjaxJson;
import com.temdemo.utils.restful.CommonServiceException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;


    @ApiOperation(value = "查询用户列表", notes = "系统管理-用户管理")
    @RequestMapping(value = "/get/userlistbypage", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public AjaxJson getUserListByPage(@RequestParam(value = "name", required = false, defaultValue = "") String name,
                                      @RequestParam(value = "accountNum", required = false, defaultValue = "") String accountNum,
                                      @RequestParam(value = "levelNum", required = false, defaultValue = "") Integer levelNum,
                                      @RequestParam(value = "companyId", required = false, defaultValue = "") Integer companyId,
                                      @RequestParam(value = "lineId", required = false, defaultValue = "") Integer lineId,
                                      @RequestParam(value = "roleId", required = false, defaultValue = "") Integer roleId,
                                      @RequestParam(value = "currentPage", required = true, defaultValue = "1") Integer currentPage,
                                      @RequestParam(value = "pageSize", required = true, defaultValue = "10") Integer pageSize){
        try {
            return  AjaxJson.success( userService.getUserListByPage(name,accountNum,levelNum,companyId,lineId,roleId,currentPage,pageSize));
        } catch (CommonServiceException e) {
            return AjaxJson.serviceException(e);
        }
    }


}
