package com.temdemo.controller;

import com.temdemo.utils.SnapPay.WXAppletApi;
import com.temdemo.utils.restful.AjaxJson;
import com.temdemo.utils.restful.CommonServiceException;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/Order")
public class OrderController {

    @ApiOperation(value = "支付回调接口", notes = "订单管理")
    @RequestMapping(value = "/notify", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public AjaxJson savePayFinish(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("访问回调");
            WXAppletApi.notify(request,response);
            return AjaxJson.success();
        }  catch (CommonServiceException e) {
            return AjaxJson.serviceException(e);
        }
    }
}
