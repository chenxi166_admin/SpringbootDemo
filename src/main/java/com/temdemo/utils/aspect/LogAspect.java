package com.temdemo.utils.aspect;

import com.temdemo.utils.restful.CommonServiceException;
import com.temdemo.utils.threadLocal.RequestHolder;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @author 晨曦
 * @title: LogAspect
 * @projectName iss-stategrid
 * @description: 日志AOP
 * @date 2019/12/914:45
 */
@Aspect
@Component
public class LogAspect {
    private final static Logger log = LoggerFactory.getLogger(LogAspect.class);
    /**
     *  定义切面
     */
    // Controller 切面
    @Pointcut("execution(public * com.temdemo.controller.*.*(..)) ")
    public void logController() {
    }
    // Service 切面
    @Pointcut("execution(public * com.temdemo.service.*.*(..))")
    public void logServie() {
    }
    //定时任务管理 切面
    @Pointcut("execution(public * com.temdemo.scheduled.*.*(..))")
    public void logScheduled() {
    }
  /*  public String getRemortIP(HttpServletRequest request) {
        if (request.getHeader("x-forwarded-for") == null) {
            return request.getRemoteAddr();
        }
        return request.getHeader("x-forwarded-for");
    }*/

    @Before(value = "logController()")
    public void doBeforeController(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        log.info("URL : " + request.getRequestURL().toString());
        log.info("HTTP_METHOD : " + request.getMethod());
        log.info("IP : " + request.getRemoteAddr());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
    }



    @Around("logServie()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object result = null;
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        try {
            result =proceedingJoinPoint.proceed();
        } catch (CommonServiceException e) {
            log.error("请求路径:" + request.getRequestURI() + "  "
                    + proceedingJoinPoint.getSignature().getDeclaringTypeName() +"   " +
                    " LineNumber --- "+e.getStackTrace()[0].getLineNumber()+ "行。  " +
                    " 发生了  : " + e.toString());
            throw new CommonServiceException(e.getCode(),e.getMessage());
        }
        return result;
    }

    @After(value = "logServie()")
    public void doAfterAdvice(){
        RequestHolder.remove();
    }


    @Around("logScheduled()")
    public Object doAroundScheduled(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object result = null;
        try {
            result = proceedingJoinPoint.proceed();
        } catch (CommonServiceException e) {
            log.error(proceedingJoinPoint.getSignature().getDeclaringTypeName() +"  " +
                    " LineNumber --- "+e.getStackTrace()[0].getLineNumber()+ "行。  " +
                    " 发生了  : " + e.toString());
            throw new CommonServiceException(e.getCode(),e.toString());
        }
        return result;
    }






}
