package com.temdemo.utils.constant;


public interface MsgConstant {

    interface CEM{
        String MISS_PARAM ="参数缺失";
        String SAVE_FAIL ="添加失败";
        String UPDATE_FAIL ="修改失败";
        String DELETE_FAIL ="删除失败";
    }
    interface LOGIN_MSG{
        String CODE_ERRO ="验证码错误";
        String CODE_INVALID ="验证码失效";
        String UNFIND_USER="用户不存在";
        String PASSWORD_ERRO ="密码错误";
    }

}
