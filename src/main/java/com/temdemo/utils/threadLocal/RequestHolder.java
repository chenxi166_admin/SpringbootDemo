package com.temdemo.utils.threadLocal;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RequestHolder {

    private final static ThreadLocal<Map<String,Object>> requestHolder = new ThreadLocal<Map<String,Object>>();

    public static void add(Map<String,Object> map) {
        requestHolder.set(map);
    }

    public static Map<String,Object> getHashMap() {
        return requestHolder.get();
    }

    public static void remove() {
        requestHolder.remove();
    }
}
