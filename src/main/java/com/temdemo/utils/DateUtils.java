package com.temdemo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * @ClassName DateUtil
 * @Description TODO
 * @Author 晨曦
 * @Date 2019/12/19 16:56
 */
public class DateUtils {

    public static Date getDateForStr(String str) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM").parse(str);
        } catch (Exception e) {
            System.out.println(e);
        }
        return date;
    }

    public static String getDateForStr(Date date) {
        String now = new SimpleDateFormat("yyyy-MM").format(date);
        return now;
    }


    public static String getDateSubtractionOneYear(String str) {
        String reStr = null;
        try {
            Date date = new SimpleDateFormat("yyyy-MM").parse(str);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.YEAR, -1);//当前时间减去一年，即一年前的时间    
            calendar.getTime();//获取一年前的时间，或者一个月前的时间  
            Date dt1 = calendar.getTime();
            reStr = sdf.format(dt1);
        } catch (Exception e) {
            System.out.println(e);
        }

        return reStr;
    }

    public static String getDateSubtractionOneMonth(String str) {
        String reStr = null;
        try {
            Date date = new SimpleDateFormat("yyyy-MM").parse(str);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, -1);//当前时间前去一个月，即一个月前的时间    
            calendar.getTime();//获取一年前的时间，或者一个月前的时间  
            Date dt1 = calendar.getTime();
            reStr = sdf.format(dt1);
        } catch (Exception e) {
            System.out.println(e);
        }
        return reStr;
    }


    public static String getUTCTime() {
       /* DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.now(ZoneOffset.UTC);
        String localTime = formatter.format(dateTime);
        return localTime;*/
        Date date = new Date();  // 对应的北京时间
        SimpleDateFormat bjSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        bjSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String datesTR = bjSdf.format(date);
        return datesTR;
    }

    public static void main(String[] args) {
        System.out.println(getUTCTime());
    }


}
