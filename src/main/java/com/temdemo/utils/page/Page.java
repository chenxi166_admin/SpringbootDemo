package com.temdemo.utils.page;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Page  {
	private Integer currentPage;//当前页
	private Integer pageSize;//每页显示行数
	private Integer totalSize=0;//总行数
	private Integer totalPage=0;//总页数
}
