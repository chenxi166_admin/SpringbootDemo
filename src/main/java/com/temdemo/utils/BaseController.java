package com.temdemo.utils;

import com.temdemo.utils.threadLocal.RequestHolder;

import java.util.Map;

public class BaseController {
    class ThreadUser {
        private String uid;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }
    }

    public static ThreadUser getLoginUserINfor() {
        Map<String, Object> hashMap = RequestHolder.getHashMap();
        ThreadUser user =new BaseController(). new ThreadUser();
        user.setUid((String) hashMap.get("id"));
        return user;
    }
}
