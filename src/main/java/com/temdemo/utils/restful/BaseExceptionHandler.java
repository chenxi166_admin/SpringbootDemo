package com.temdemo.utils.restful;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
@Slf4j
public class BaseExceptionHandler {

    @ExceptionHandler(CommonServiceException.class)
    @ResponseBody
    public AjaxJson serviceExceptionHandler(
            HttpServletRequest request, CommonServiceException e){
        log.error("CommonServiceException, code:"+ e.getCode() +", message : "+ e.getMessage());
        return AjaxJson.serviceException(e);
    }

}
