package com.temdemo.utils.restful;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * Ajax返回信息实体
 *
 * @author AsherLi
 * @version V1.0.00
 */
@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class AjaxJson implements Serializable {

    /**
     * 错误码
     */
    private Integer status = 200;
    /**
     * 操作是否成功
     */
    private boolean success = true;
    /**
     * 提示信息
     */
    private String msg = "操作成功";
    /**
     * 返回数据
     */
    private Object data = null;

    private Object obj =null;

    /**
     * 返回其他参数
     */
    private Map<String, Object> attributes;
    // 成功但是无参数
    public static AjaxJson success(){
        return new AjaxJson(200,true,"操作成功",null,null,null);
    }

    // 成功有参数
    public static<M> AjaxJson success(M data){
        return  new AjaxJson(200,true,"操作成功",data,null,null);
    }

    // 出现业务异常
    public static<M> AjaxJson serviceException(CommonServiceException e){
        if(e.getCode()==null){
            return new AjaxJson(500,false,"操作失败",null,null,null);
        }else{
            return new AjaxJson(e.getCode(),false,e.getMessage(),null,null,null);
        }
    }


}