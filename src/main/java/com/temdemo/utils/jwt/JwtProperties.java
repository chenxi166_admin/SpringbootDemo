package com.temdemo.utils.jwt;

/**
 * @author 晨曦
 * @title: JwtProperties
 * @projectName iss-stategrid
 * @description: JWT 配置 文件
 * @date 2019/12/913:23
 */
public class JwtProperties {

    private static JwtProperties jwtProperties = new JwtProperties();
    private JwtProperties(){}
    public static JwtProperties getJwtProperties(){
        return jwtProperties;
    }

    public static final String JWT_PREFIX = "jwt";

    private String header = "Authorization";

    private String secret = "MDk4ZjZiIxHPT7BFV67XM2NYYYU0ZTgzMjYyN2I0ZjY";

    private Long expiration = 604800L; // 默认是七天

    private String authPath = "login";

    private String md5Key = "randomKeySH512";

    public static String getJwtPrefix() {
        return JWT_PREFIX;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Long getExpiration() {
        return expiration;
    }

    public void setExpiration(Long expiration) {
        this.expiration = expiration;
    }

    public String getAuthPath() {
        return authPath;
    }

    public void setAuthPath(String authPath) {
        this.authPath = authPath;
    }

    public String getMd5Key() {
        return md5Key;
    }

    public void setMd5Key(String md5Key) {
        this.md5Key = md5Key;
    }
}