package com.temdemo.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.temdemo.dao.SystemUserMapper;
import com.temdemo.entity.SystemUser;
import com.temdemo.service.UserService;
import com.temdemo.utils.page.PageForm;
import com.temdemo.utils.restful.CommonServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private SystemUserMapper systemUserMapper;

    @Override
    public PageForm getUserListByPage(String name, String accountNum, Integer levelNum, Integer companyId, Integer lineId, Integer roleId, Integer currentPage, Integer pageSize) throws CommonServiceException {
        PageForm pageForm = new PageForm();
        Map map = new HashMap();
        map.put("name",name);
        map.put("accountNum",accountNum);
        map.put("levelNum",levelNum);
        map.put("companyId",companyId);
        map.put("lineId",lineId);
        map.put("roleId",roleId);
        Page page = PageHelper.startPage(currentPage, pageSize, true);
        Page<SystemUser> list = (Page<SystemUser>) systemUserMapper.getUserListByPage(map);
        //PageInfo<SystemUser> pageInfo = new PageInfo<>(list);
        com.temdemo.utils.page.Page page1 = new  com.temdemo.utils.page.Page()
                .setTotalSize((int) page.getTotal())
                .setTotalPage(page.getPages())
                .setPageSize(pageSize)
                .setCurrentPage(currentPage);
        pageForm.setList(list);
        pageForm.setPage(page1);
        return pageForm;
    }
}
