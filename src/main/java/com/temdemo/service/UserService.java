package com.temdemo.service;

import com.temdemo.utils.page.PageForm;
import com.temdemo.utils.restful.CommonServiceException;

public interface UserService {
     PageForm getUserListByPage(String name, String accountNum, Integer levelNum, Integer companyId, Integer lineId, Integer roleId, Integer currentPage, Integer pageSize) throws CommonServiceException;
}
