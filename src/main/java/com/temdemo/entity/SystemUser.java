package com.temdemo.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class SystemUser {
    private Integer id;

    private String userName;

    private String accountNum;

    private String password;

    private String salt;

    private Integer level;

    private Integer roleId;

    private String companyId;

    private String lineId;

    private Integer addUser;

    private Date updateAt;

    private Integer state;

    private Integer isDelete;

    private Date creatAt;


}